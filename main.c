/* 
 * File:   main.c
 * Author: Vepris
 *
 * Created on pirmdiena, 2013, 21 janvāris, 21:20
 */
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

#include <pps.h>


#ifndef FCY
    #define FCY  10000000   // FCY is 20/2 =  10 MHz
#endif
#include <libpic30.h>

//#include <xlcd.h>
#include "lcd.h"
#include "ds18b20.h"

#include <TCPIP.h>
#include "main.h"

#pragma config FNOSC=PRI, IESO=OFF              // FOSCSEL. This specifies initial clock source at POR. Disable PLL.
#pragma config POSCMD=HS, IOL1WAY=ON, FCKSM=CSDCMD  // FOSC. HS Crystal Oscillator mode . Clock switching disabled. Peripheral Pin Select Configuration: Allow Only One Re-configuration
#pragma config FWDTEN=OFF, WINDIS=OFF    // WDT. Disabled. Non-window mode.
#pragma config FPWRT=PWR128         // FPOR. POR 128ms.
#pragma config ICS=PGD1, JTAGEN=OFF // FICD

// FBS
#pragma config BWRP = WRPROTECT_OFF     // Boot Segment Write Protect (Boot Segment may be written)
//#pragma config BSS = NO_FLASH           // Boot Segment Program Flash Code Protection (No Boot program Flash segment)
//#pragma config RBS = NO_RAM             // Boot Segment RAM Protection (No Boot RAM)


#define LED1_TRIS  TRISBbits.TRISB5

// pg.112, Bit instructions
#define bit_set(var,bitno) ((var) |= 1 << (bitno))
#define bit_clr(var,bitno) ((var) &= ~(1 << (bitno)))
#define testbit_on(data,bitno) ((data>>bitno)&0x01)
#define READ_PIN_DQ PORTBbits.RB2

// Declare AppConfig structure and some other supporting stack variables
BYTE AN0String[8];
APP_CONFIG AppConfig;

int writeSPI(int);
void InitAppConfig(void);
void ApplicationTask(void);

static void InitializeBoard(void);

void processsTemperature(unsigned char*);
void measureTemperature(void);


enum STATES
{
    STATE_NEW,
    STATE_WAIT,
    STATE_MEASURE
} qAppState; // state tracking var

int new_temperature = 0;
unsigned char scratchpad[2][9];


unsigned char sensors[][8] = {
    {0x28, 0xc5, 0x9b, 0x03, 0x03, 0x00, 0x00, 0xc1},  // outside
    {0x28, 0xda, 0xa7, 0x03, 0x03, 0x00, 0x00, 0x1b} // inside
};


int main(void) {

    /** HARDWARE Init **/
    InitializeBoard();

    LED1_TRIS = 0;
    LED1 = 0;

/**
	// EEPROM Writes. Program as 11 bit resolution
	ds18_dallas_init();
	ds18_write_byte(0xCC); // Skip ROM
	ds18_write_byte(0x4E); // Write Scratchpad
	ds18_write_byte(0xFF); // Th Register
	ds18_write_byte(0xFF); // Tl Register
	ds18_write_byte(0x5F); // 11 bit Config
	ds18_dallas_init();
	ds18_write_byte(0xCC); // Skip ROM
	ds18_write_byte(0x48); // Copy scratchpad

	while(1){

	}
*/

    lcd_init();
    lcd_goto(0x00);

    
    LED1 = 1;
    TickInit(); // p.161. Init tick timer that manages stack timing.
    #ifdef STACK_USE_MPFS2
        MPFSInit();
    #endif

    // Hardware is now initialized.
    /** END HARDWARE Init **/


    /** Begin configuring STACK **/
    
    InitAppConfig(); // Init AppConfig struct

    /** End config STACK */



    /**
     * This will automatically call the initialization functions for other firmware protocols
     * if they have been enabled in TCPIPConfig.h (i.e. TCPInit() for the TCP protocol, HTTPInit() for HTTP2,...)
     */
    StackInit();




    qAppState = STATE_NEW;



    while(1){
        // each subtask is implemented as state-machine controlled cooperative multitasking function.
        // This task performs normal stack task including checking for incoming packet, type of packet and calling appropriate stack entity to process it.
        StackTask(); // each call of StackTask can retrieve one packet from the packet buffer
        StackApplications(); // This will call loaded application modules.
        ApplicationTask();
        
        
    }

    return (EXIT_SUCCESS);
}


void ApplicationTask(){
    static DWORD tickCounter; // 32-bits unsigned

    switch (qAppState){
        case STATE_NEW:

            tickCounter = TickGet() + (5*TICK_SECOND);
            ++qAppState;
            break;
        case STATE_WAIT:
            if ((long)(TickGet() - tickCounter) > 0) // time to measure
                ++qAppState;
            break;

        case STATE_MEASURE:

            //@TODO measureTemperature cooperative multitasking
            measureTemperature();
            LED1 = ~LED1;
            // Measuring finished. Start new cycle.
            qAppState = STATE_NEW;
            break;

    }

}


// Send one byte of data and receive one back at the same time
int writeSPI(int data){
    SPI1BUF = data; // write to buffer for TX
    while(!SPI1STATbits.SPIRBF); // wait for transfer to complete

    return SPI1BUF;
}



/********************************************************************
 * APP_CONFIG structure
 * PreCondition:    MPFSInit() is already called.
 * Output:          Write/Read non-volatile config variables.
 *
 * Addresses p.171, flags,  NBNS/SSID
 *******************************************************************/
void InitAppConfig(void){

    // Start out zeroing all AppConfig bytes to ensure all fields are
    // deterministic for checksum generation
    memset((void*)&AppConfig, 0x00, sizeof(AppConfig));

    AppConfig.Flags.bIsDHCPEnabled = TRUE;
    AppConfig.Flags.bInConfigMode = TRUE;

    AppConfig.MyMACAddr.v[0] = MY_DEFAULT_MAC_BYTE1;
    AppConfig.MyMACAddr.v[1] = MY_DEFAULT_MAC_BYTE2;
    AppConfig.MyMACAddr.v[2] = MY_DEFAULT_MAC_BYTE3;
    AppConfig.MyMACAddr.v[3] = MY_DEFAULT_MAC_BYTE4;
    AppConfig.MyMACAddr.v[4] = MY_DEFAULT_MAC_BYTE5;
    AppConfig.MyMACAddr.v[5] = MY_DEFAULT_MAC_BYTE6;
    //memcpypgm2ram((void*)&AppConfig.MyMACAddr, (ROM void*)SerializedMACAddress, sizeof(AppConfig.MyMACAddr));

    AppConfig.MyIPAddr.Val = MY_DEFAULT_IP_ADDR_BYTE1 | MY_DEFAULT_IP_ADDR_BYTE2<<8ul | MY_DEFAULT_IP_ADDR_BYTE3<<16ul | MY_DEFAULT_IP_ADDR_BYTE4<<24ul;
    AppConfig.DefaultIPAddr.Val = AppConfig.MyIPAddr.Val;
    AppConfig.MyMask.Val = MY_DEFAULT_MASK_BYTE1 | MY_DEFAULT_MASK_BYTE2<<8ul | MY_DEFAULT_MASK_BYTE3<<16ul | MY_DEFAULT_MASK_BYTE4<<24ul;
    AppConfig.DefaultMask.Val = AppConfig.MyMask.Val;
    AppConfig.MyGateway.Val = MY_DEFAULT_GATE_BYTE1 | MY_DEFAULT_GATE_BYTE2<<8ul | MY_DEFAULT_GATE_BYTE3<<16ul | MY_DEFAULT_GATE_BYTE4<<24ul;
    AppConfig.PrimaryDNSServer.Val = MY_DEFAULT_PRIMARY_DNS_BYTE1 | MY_DEFAULT_PRIMARY_DNS_BYTE2<<8ul  | MY_DEFAULT_PRIMARY_DNS_BYTE3<<16ul  | MY_DEFAULT_PRIMARY_DNS_BYTE4<<24ul;
    AppConfig.SecondaryDNSServer.Val = MY_DEFAULT_SECONDARY_DNS_BYTE1 | MY_DEFAULT_SECONDARY_DNS_BYTE2<<8ul  | MY_DEFAULT_SECONDARY_DNS_BYTE3<<16ul  | MY_DEFAULT_SECONDARY_DNS_BYTE4<<24ul;


}




static void InitializeBoard(void)
{


    // Configure ANx as Digital pins
    AD1PCFGL = 0x1E3F;
    AD1CON1bits.ADON = 0; // ADC off
    TRISBbits.TRISB5 = 0; // output

    OSCCONbits.NOSC = 0b010; // New oscillator selection bits
    OSCCONbits.CLKLOCK = 1;  // Disable clock switching

    CLKDIVbits.DOZEN = 1;   // Doze mode enabled
    CLKDIVbits.DOZE = 0b000; // Fcy divided by 1

    // SPI Config
    SPI1STATbits.SPIEN = 1; // Enable module
    SPI1CON1bits.MSTEN = 1; // Master mode
    SPI1CON1bits.CKE = 1;
    SPI1CON1bits.CKP = 0;

    /**** PPS Remap. SPI Setup. *****/

    // Unlock OSCCON.IOLOCK
    PPSUnLock;
    // Inputs
    TRISBbits.TRISB9 = 1;
    RPINR20 = 0b0001111100001001; // SDI1 tied to RP9
    // Outputs
    RPOR3 = 0b0000100000001001; // SCK1 is assigned to RP7, RP6 tied to SPI1 Slave Select Output
    RPOR4 = 0b0000000000000111; // RP8 tied to SPI1 Data Output
    PPSLock;

#if defined(SPIRAM_CS_TRIS)
    SPIRAMInit();
#endif
#if defined(EEPROM_CS_TRIS)
    XEEInit();
#endif
#if defined(SPIFLASH_CS_TRIS)
    SPIFlashInit();
#endif
}


void processsTemperature(unsigned char *scratchpad){

    unsigned char scratchpad_lsb, scratchpad_msb;

    scratchpad_lsb = scratchpad[0];
    scratchpad_msb = scratchpad[1];


    // Subzero Celsius
    if (testbit_on(scratchpad_msb, 7)) {
        // fikseejam, ka ir minus
        lcd_putch(0b00101101); // minus sign
        // ja ir minus, tad vajag complement un +1
        // http://www.mikroe.com/forum/viewtopic.php?f=10&t=17810
        scratchpad_lsb = ~scratchpad_lsb;
        scratchpad_lsb++;
        scratchpad_msb = ~scratchpad_msb;
    } else
        lcd_putch(0b00101011); // plus sign

    unsigned char temperature = 0;
    temperature = scratchpad_lsb >> 4;
    temperature |= (scratchpad_msb << 4);
    bit_clr(temperature, 7);




    // tie kas aiz komata. unsigned short taadeelj, ka var buut liidz 999, taadeelj vajag 16-bit variable
    unsigned short decimals = 0;
    // vispirms njemam cik ir, peectam reizinaasim
    // njemam tikai tos kas ir aiz komata no lsb
    decimals = ~0b11110000 & scratchpad_lsb;
    // bit 0 do not count
    decimals >>= 1;
    decimals *= 125;

    unsigned char hundreds = 0;
    while (decimals >= 100) {
        decimals -= 100;
        hundreds++;
    }


    lcd_writeNumber(temperature);
    lcd_putch(0b00101110);
    lcd_putch(hundreds + 48);

}



void measureTemperature(void){


    ds18_dallas_init();
   
    /**
    int d = 0;
    // Determine 64-bit Lasered ROM address. Use Debugger to read value.
    ds18_write_byte(0x33); // Read ROM

    unsigned char scratchpad_0, scratchpad_1, scratchpad_2, scratchpad_3, scratchpad_4, scratchpad_5, scratchpad_6, scratchpad_7;

    scratchpad_0 = ds18_read_byte(); // LSB
    scratchpad_1 = ds18_read_byte(); 
    scratchpad_2 = ds18_read_byte(); 
    scratchpad_3 = ds18_read_byte();
    scratchpad_4 = ds18_read_byte();
    scratchpad_5 = ds18_read_byte();
    scratchpad_6 = ds18_read_byte();
    scratchpad_7 = ds18_read_byte();
   // scratchpad_8 = ds18_read_byte();

    ++d;
    ++d;
    ++d;
    **/


    ds18_write_byte(0xCC); // Skip ROM
    ds18_write_byte(0x44); // Convert T for all Sensors

    // Only if external power supply is used

    
    // issue Read time slots after the Convert T command. Sensor will hold Line LOW while the temperature conversion is in progress.
    while (!ds18_read_slot()) {
    }

    int i,j;
    int sensors_count = sizeof(sensors)/sizeof(*sensors);


    for (i=0; i<sensors_count; ++i){
        ds18_dallas_init();
        ds18_write_byte(0x55); // Match ROM. Sensor #1
        // send 64bit ROM code

        for (j=0; j<sizeof(sensors[i]); ++j){
            ds18_write_byte(sensors[i][j]);
        }

        ds18_write_byte(0xBE); // Read scratchpad

        for (j=0; j<sizeof(*scratchpad); ++j){
            scratchpad[i][j] = ds18_read_byte();
        }


        switch (i){
            case 0:
                lcd_goto(0x00);
                break;
            case 1:
                lcd_goto(0x40);
                break;
        }
  
        //    processsTemperature(scratchpad_0, scratchpad_1);
        processsTemperature(scratchpad[i]);

    }


    

    




    



    new_temperature = 1;
    // Reset
    //__delay_us(2);
}