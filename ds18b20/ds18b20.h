/**
 * 
 */
#ifndef DS18B20_H_
#define DS18B20_H_

/**
 * Init DS18B20
 */
extern void ds18_dallas_init(void);

extern void ds18_write_byte(unsigned char);

extern unsigned char ds18_read_byte(void);

extern int ds18_read_slot(void);



#endif /* DS18B20_H_ */
