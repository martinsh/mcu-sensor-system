/**
 * ds18b20.c
 *
 * RA3 - DS18B20 Bus
 */
#include	<xc.h>
#include	"ds18b20.h"

#define FCY  10000000   // Fcy is 20/2 =  10 MHz
#include <libpic30.h>   // For timing delays



// Thermometer Data PIN
#define DQ LATBbits.LATB3
#define DQ_READ_PIN PORTBbits.RB3

#define DQ_DIR TRISBbits.TRISB3
#define DQ_HIGH() DQ_DIR =1
#define DQ_LOW() DQ = 0; DQ_DIR = 0

/**
 * Init DS18B20
 *
 */
void ds18_dallas_init()
{
	char presence = 1;
	while (presence)
	{
		presence = 1;
		DQ_LOW() ;
                __delay_us(503); //503us

                // Release the bus and go into Rx Mode
		DQ_HIGH();      // wait 
                __delay_us(70); //70us
		
		if (!DQ_READ_PIN)
			presence = 0;
                __delay_us(430); //430us
	}
}



unsigned char ds18_read_byte()
{
	unsigned char i;
	unsigned char value = 0;
        int slot_rez;

        // Every read slot must be a minimum of 60us.
	for (i=0; i<8; i++)
	{
		value >>= 1;

                slot_rez = ds18_read_slot();
                if (slot_rez == 1)
                    value |= 0b10000000;

                
	}
	return (value);
}


int ds18_read_slot(void){
    int ret = 0;

    DQ_LOW();       // initiate read time slot by pulling line LOW for a min of 1us.
    __delay_us(2);

    DQ_HIGH();
    __delay_us(5); // We must sample the bus state within 15us from the start of the slot.

    if (DQ_READ_PIN)
        ret |= 0x1;
    __delay_us(58); // Total slot width: min 60us.
    return ret;
}



void ds18_write_byte(unsigned char val)
{
	unsigned char i;
	unsigned char temp;
	for (i = 8;i > 0;i--)
	{
		temp = val & 0x01;
		DQ_LOW(); // initiate write time slot.
                __delay_us(5);

		if (temp == 1) // if Write 1 Slot, release the line within 15us.
			DQ_HIGH();
                __delay_us(63); // if Write 0 slot, hold the line low for at least 60us

		DQ_HIGH();

                __delay_us(2); // Minimum 1us recovery time between slots.
		
		val = val >> 1;     
	}
}

