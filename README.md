pic24tcpip
==========

PIC24 TCP/IP device firmware.
Project Goal: Get temperature readings over the Internet.

Target MCU: Microchip PIC24HJ128GP502. 
Ethernet interface: ENC28J60.
LCD: HD44780
Temperature sensor: DS18B20

Compiler: XC16