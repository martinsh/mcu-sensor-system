/* 
 * File:   main.h
 * Author: Vepris
 *
 * Created on otrdiena, 2013, 16 aprīlis, 22:23
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif



#define LED1 LATBbits.LATB5
extern int new_temperature;
extern unsigned char scratchpad[2][9], sensors[2][8];


#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */

