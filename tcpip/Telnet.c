/*********************************************************************
 *
 *	Telnet Server
 *  Module for Microchip TCP/IP Stack
 *	 -Provides Telnet services on TCP port 23
 *	 -Reference: RFC 854
 *
 *********************************************************************
 * FileName:        Telnet.c
 * Dependencies:    TCP
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.05 or higher
 *					Microchip C30 v3.12 or higher
 *					Microchip C18 v3.30 or higher
 *					HI-TECH PICC-18 PRO 9.63PL2 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 *
 * Author               Date    Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Howard Schlunder     9/12/06	Original
 ********************************************************************/
#define __TELNET_C

#include "TCPIPConfig.h"

#if defined(STACK_USE_TELNET_SERVER)

#include "TCPIP.h"
#include "main.h"

// Set up configuration parameter defaults if not overridden in 
// TCPIPConfig.h
#if !defined(TELNET_PORT)
    // Unsecured Telnet port
	#define TELNET_PORT			23
#endif
#if !defined(TELNETS_PORT)	
    // SSL Secured Telnet port (ignored if STACK_USE_SSL_SERVER is undefined)
	#define TELNETS_PORT		992	
#endif
#if !defined(MAX_TELNET_CONNECTIONS)
    // Maximum number of Telnet connections
	#define MAX_TELNET_CONNECTIONS	(3u)
#endif
#if !defined(TELNET_USERNAME)
    // Default Telnet user name
	#define TELNET_USERNAME		"admin"
#endif
#if !defined(TELNET_PASSWORD)
    // Default Telnet password
	#define TELNET_PASSWORD		"microchip"
#endif

// Demo title string
static const BYTE strTitle[]			= "\x1b[2J\x1b[34m\x1b[1m"	// 2J is clear screen, 31m is red, 1m is bold
									  "Microchip Telnet Server 1.1\x1b[0m\r\n"	// 0m is clear all attributes
								  	  "Login: ";
// Demo password
static const BYTE strPassword[]		= "Password: \xff\xfd\x2d";	// DO Suppress Local Echo (stop telnet client from printing typed characters)
// Access denied message
static const BYTE strAccessDenied[]	= "\r\nAccess denied\r\n\r\n";
// Successful authentication message
static const BYTE strAuthenticated[]	= "\r\nHello!\r\n\r\n"
									  "\r\nPress 'q' to quit\r\n";

// String with extra spaces, for Demo
static const BYTE strSpaces[]			= "          ";
// Demo disconnection message
static const BYTE strGoodBye[]		= "\r\n\r\nGoodbye!\r\n";

extern BYTE AN0String[8];



/*********************************************************************
 * Function:        void TelnetTask(void)
 *
 * PreCondition:    Stack is initialized()
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        Performs Telnet Server related tasks.  Contains
 *                  the Telnet state machine and state tracking
 *                  variables.
 *
 * Note:            None
 ********************************************************************/
void TelnetTask(void)
{
	BYTE 		i;
	BYTE		vTelnetSession;
	WORD		w, w2, rxBufferLength;
	TCP_SOCKET	MySocket;
	enum STATES
	{
		SM_HOME = 0,
		SM_PRINT_LOGIN,
		SM_GET_LOGIN,
		SM_GET_PASSWORD,
		SM_GET_PASSWORD_BAD_LOGIN,
		SM_AUTHENTICATED,
		SM_REFRESH_VALUES
	} ;
        enum STATES TelnetState;
        
	static TCP_SOCKET hTelnetSockets[MAX_TELNET_CONNECTIONS];
	static BYTE vTelnetStates[MAX_TELNET_CONNECTIONS];
	static BOOL bInitialized = FALSE;

	// Perform one time initialization on power up
	if(!bInitialized)
	{
		for(vTelnetSession = 0; vTelnetSession < MAX_TELNET_CONNECTIONS; vTelnetSession++)
		{
			hTelnetSockets[vTelnetSession] = INVALID_SOCKET; // byte
			vTelnetStates[vTelnetSession] = SM_HOME;
		}
		bInitialized = TRUE;
	}


	// Loop through each telnet session and process state changes and TX/RX data
	for(vTelnetSession = 0; vTelnetSession < MAX_TELNET_CONNECTIONS; vTelnetSession++)
	{
		// Load up static state information for this session
		MySocket = hTelnetSockets[vTelnetSession]; // byte
		TelnetState = vTelnetStates[vTelnetSession];

		// Reset our state if the remote client disconnected from us
		if(MySocket != INVALID_SOCKET)
		{
			if(TCPWasReset(MySocket))
				TelnetState = SM_PRINT_LOGIN;
		}
	
		// Handle session state
		switch(TelnetState)
		{
			case SM_HOME:
				// Connect a socket to the remote TCP server. Open a server socket
				MySocket = TCPOpen(0, TCP_OPEN_SERVER, TELNET_PORT, TCP_PURPOSE_TELNET);
				
				// Abort operation if no TCP socket of type TCP_PURPOSE_TELNET is available
				// If this ever happens, you need to go add one to TCPIPConfig.h
				if(MySocket == INVALID_SOCKET)
					break;
	
				// Open an SSL listener if SSL server support is enabled
				#if defined(STACK_USE_SSL_SERVER)
					TCPAddSSLListener(MySocket, TELNETS_PORT);
				#endif
	
				TelnetState++;
				break;
	
			case SM_PRINT_LOGIN:
				#if defined(STACK_USE_SSL_SERVER)
					// Reject unsecured connections if TELNET_REJECT_UNSECURED is defined
					#if defined(TELNET_REJECT_UNSECURED)
						if(!TCPIsSSL(MySocket))
						{
							if(TCPIsConnected(MySocket))
							{
								TCPDisconnect(MySocket);
								TCPDisconnect(MySocket);
								break;
							}	
						}
					#endif
						
					// Don't attempt to transmit anything if we are still handshaking.
					if(TCPSSLIsHandshaking(MySocket))
						break;
				#endif
				
				// Make certain the socket can be written to. Return to main loop if not ready
				if(TCPIsPutReady(MySocket) < strlen((const char*)strTitle))
					break;
				
				// Place the application protocol data into the transmit buffer.
				TCPPutROMString(MySocket, strTitle);
	
				// Send the packet
				TCPFlush(MySocket);
				TelnetState++;
	
			case SM_GET_LOGIN:
				// Make sure we can put the password prompt
				if(TCPIsPutReady(MySocket) < strlen((const char*)strPassword))
					break;
	
				// See if the user pressed return
				//w = TCPFind(MySocket, '\n', 0, FALSE);
                                w = TCPFindEx(MySocket, '\n', 0, 0, FALSE); // FALSE for binary searcg

				if(w == 0xFFFFu) // Search array not found 
				{
					if(TCPGetRxFIFOFree(MySocket) == 0u)
					{
						TCPPutROMString(MySocket, (const BYTE*)"\r\nToo much data.\r\n");
						TCPDisconnect(MySocket); // return server socket to the listening state
					}
	
					break;
				}

                                

                                // search for telnet IAC commands. Strip these commands.
                                w2 = TCPFindEx(MySocket, 0xFF, 0, 0, FALSE);
                                BYTE iac[3];
                                while (w2 == 0) {
                                    TCPGetArray(MySocket, iac, 3); // Commands occupies 3 bytes
                                    // Here we can process Telnet commands but we wont
                                    w2 = TCPFindEx(MySocket, 0xFF, 0, 0, FALSE);
                                }




				// Search for the username -- case insensitive in the TCP RX buffer
				w2 = TCPFindArrayEx(MySocket, (const BYTE*)TELNET_USERNAME, sizeof(TELNET_USERNAME)-1, 0, 0, TRUE);
                                //w2 = TCPFindROMArrayEx(MySocket, (const BYTE*)TELNET_USERNAME, sizeof(TELNET_USERNAME)-1, 0, 0, TRUE);
				//if( !((sizeof(TELNET_USERNAME)-1 == w - w2) || (sizeof(TELNET_USERNAME) == w - w2)))

				// Throw this line of data away
                                rxBufferLength = TCPIsGetReady(MySocket);
                                // buffer content example: 'a','d','m','i','n','\r','\n'
				TCPGetArray(MySocket, NULL, rxBufferLength);

                                /**
                                 *
                                // for debug
                                WORD aaa;
                                aaa = TCPIsGetReady(MySocket);

                                BYTE buff[35];
                                TCPGetArray(MySocket, buff, aaa);
                                */


                                if( w2 == 0 && rxBufferLength == (1+sizeof(TELNET_USERNAME)) ) // sizeof('admin') = 6 (includes \n)
				{
                                    TelnetState = SM_GET_PASSWORD;
				}
				else
				{
                                    // Did not find the username, but let's pretend we did so we don't leak the user name validity
                                    TelnetState = SM_GET_PASSWORD_BAD_LOGIN;
				}

	
				// Print the password prompt
				TCPPutROMString(MySocket, strPassword);
				TCPFlush(MySocket);
				break;
	
			case SM_GET_PASSWORD:
			case SM_GET_PASSWORD_BAD_LOGIN:
				// Make sure we can put the authenticated prompt
				if(TCPIsPutReady(MySocket) < strlen((const char*)strAuthenticated))
					break;
	
				// See if the user pressed return
				w = TCPFindEx(MySocket, '\n', 0, 0, FALSE);
				if(w == 0xFFFFu)
				{
					if(TCPGetRxFIFOFree(MySocket) == 0u)
					{
						TCPPutROMString(MySocket, (const BYTE*)"Too much data.\r\n");
						TCPDisconnect(MySocket);
					}
	
					break;
				}


                                // Strip Telnet commands
                                w2 = TCPFindEx(MySocket, 0xFF, 0, 0, FALSE);
                                while (w2 == 0) {
                                    TCPGetArray(MySocket, iac, 3); // Commands occupies 3 bytes
                                    w2 = TCPFindEx(MySocket, 0xFF, 0, 0, FALSE);
                                }




				// Search for the password -- case sensitive
                                w2 = TCPFindArrayEx(MySocket, (const BYTE*)TELNET_PASSWORD, sizeof(TELNET_PASSWORD)-1, 0, 0, FALSE);


                                // Remove from FIFO
                                rxBufferLength = TCPIsGetReady(MySocket);
                                TCPGetArray(MySocket, NULL, rxBufferLength);
                                

                                //if(!((sizeof(TELNET_PASSWORD) == w - w2) || (sizeof(TELNET_PASSWORD) - 1 == w - w2))  || (TelnetState == SM_GET_PASSWORD_BAD_LOGIN))
                                if( TelnetState == SM_GET_PASSWORD_BAD_LOGIN || w2 == 0xFFFF || rxBufferLength != (1+sizeof(TELNET_PASSWORD))  )
                                {
					// Did not find the password
					TelnetState = SM_PRINT_LOGIN;
					TCPPutROMString(MySocket, strAccessDenied);
					TCPDisconnect(MySocket);
					break;
				}

	
				// Print the authenticated prompt
				TCPPutROMString(MySocket, strAuthenticated);
				TelnetState = SM_AUTHENTICATED;
				// No break
		
			case SM_AUTHENTICATED:

				++TelnetState;
	
				// All future characters will be bold
				TCPPutROMString(MySocket, (const BYTE*)"\x1b[1m"); // [1m - bold
	
			case SM_REFRESH_VALUES:

                            if (!new_temperature)
                                break;

				if(TCPIsPutReady(MySocket) >= 78u)
				{
					
	
					// Position cursor at Line 11, Col 21
		//			TCPPutROMString(MySocket, (const BYTE*)"\x1b[11;21f");
	
					// Put analog value with space padding on right side for 4 characters
					//TCPPutROMArray(MySocket, (const BYTE*)strSpaces, 4-strlen((char*)AN0String));
					//TCPPutString(MySocket, AN0String);

                                        int i,s;



                                        unsigned int sensor_count = sizeof sensors/sizeof (*sensors);
                                        for (s=0; s<sensor_count; ++s){

                                            // Command identifier. Free of choice.
                                            TCPPut(MySocket, '\xB6'); //0xB6 - new temperature incoming


                                            // Sensor identifier
                                            for (i=0; i<sizeof(sensors[s]); ++i){
                                                TCPPut(MySocket, sensors[s][i]);
                                            }


                                            // Write scratchpad to the Stream
                                            for (i=0; i<sizeof(scratchpad[s]); ++i){
                                                TCPPut(MySocket, scratchpad[s][i]);
                                            }

                                        }




                                        
					
                                        /**
					TCPPut(MySocket, ' ');
					TCPPut(MySocket, '\x3C');
					TCPPut(MySocket, ' ');
					TCPPut(MySocket, '\x67');
					TCPPut(MySocket, ' ');
					TCPPut(MySocket, LED1?'0':'1');
*/
                                        new_temperature = 0;
		
					// Put LEDs
					//TCPPutROMString(MySocket, (const BYTE*)"\x1b[13;10f");
                                        /**
					TCPPut(MySocket, LED7_IO ? '1':'0');
					TCPPut(MySocket, ' ');
					TCPPut(MySocket, LED6_IO ? '1':'0');
					TCPPut(MySocket, ' ');
					TCPPut(MySocket, LED5_IO ? '1':'0');
					TCPPut(MySocket, ' ');
					TCPPut(MySocket, LED4_IO ? '1':'0');
					TCPPut(MySocket, ' ');
					TCPPut(MySocket, LED3_IO ? '1':'0');
					TCPPut(MySocket, ' ');
					TCPPut(MySocket, LED2_IO ? '1':'0');
					TCPPut(MySocket, ' ');
					TCPPut(MySocket, LED1_IO ? '1':'0');
					TCPPut(MySocket, ' ');
					TCPPut(MySocket, LED0_IO ? '1':'0');
                                        */
		
					// Put cursor at beginning of next line
					//TCPPutROMString(MySocket, (const BYTE*)"\x1b[14;1f");
	
					// Send the data out immediately
					TCPFlush(MySocket);
				}
	
				if(TCPIsGetReady(MySocket))
				{
					TCPGet(MySocket, &i);
					switch(i)
					{
						case '\r':
						case 'q':
						case 'Q':
							if(TCPIsPutReady(MySocket) >= strlenpgm((const char*)strGoodBye))
								TCPPutROMString(MySocket, strGoodBye);
							TCPDisconnect(MySocket);
							TelnetState = SM_PRINT_LOGIN;							
							break;
					}
				}
	
				break;
		}


		// Save session state back into the static array
		hTelnetSockets[vTelnetSession] = MySocket;
		vTelnetStates[vTelnetSession] = TelnetState;
	}
}

#endif	//#if defined(STACK_USE_TELNET_SERVER)
