#ifndef HARDWARE_PROFILE_H
#define HARDWARE_PROFILE_H

#include "Compiler.h"

// Define a macro describing this hardware set up (used in other files)
#define INTERNET_RADIO





// ENC28J60 I/O pins
#define ENC_CS_TRIS		(TRISBbits.TRISB6)	// Comment this line out if you are using the ENC424J600/624J600, MRF24WB0M, or other network controller.
#define ENC_CS_IO		(LATBbits.LATB6)
//#define ENC_RST_TRIS		(TRISDbits.TRISD15)	// Not connected by default.  It is okay to leave this pin completely unconnected, in which case this macro should simply be left undefined.
//#define ENC_RST_IO			(PORTDbits.RD15)
// SPI SCK, SDI, SDO pins are automatically controlled by the
// PIC24/dsPIC/PIC32 SPI module
#define ENC_SCK_TRIS		(TRISBbits.TRISB7)
#define ENC_SDI_TRIS		(TRISBbits.TRISB9)
#define ENC_SDO_TRIS		(TRISBbits.TRISB8)
// zemaakie ir ok, augseejie nav
#define ENC_SPI_IF		(IFS0bits.SPI1IF) // SPI1 Event Interrupt Flag Status bit

#define ENC_SSPBUF		(SPI1BUF)
#define ENC_SPISTAT		(SPI1STAT)
#define ENC_SPISTATbits		(SPI1STATbits)
#define ENC_SPICON1		(SPI1CON1)
#define ENC_SPICON1bits		(SPI1CON1bits)
#define ENC_SPICON2		(SPI1CON2)


// Clock frequency values
// These directly influence timed events using the Tick module.  They also are used for UART and SPI baud rate generation.
// p.165
#define GetSystemClock()		(20000000ul)			// Hz
#define GetInstructionClock()	(GetSystemClock()/2)	// Normally GetSystemClock()/4 for PIC18, GetSystemClock()/2 for PIC24/dsPIC, and GetSystemClock()/1 for PIC32.  Might need changing if using Doze modes.
#define GetPeripheralClock()	(GetSystemClock()/2)	// Normally GetSystemClock()/4 for PIC18, GetSystemClock()/2 for PIC24/dsPIC, and GetSystemClock()/1 for PIC32.  Divisor may be different if using a PIC32 since it's configurable.


// UART mapping functions for consistent API names across 8-bit and 16 or
// 32 bit compilers.  For simplicity, everything will use "UART" instead
// of USART/EUSART/etc.

/**
#define BusyUART()			BusyUSART()
#define CloseUART()			CloseUSART()
#define ConfigIntUART(a)	ConfigIntUSART(a)
#define DataRdyUART()		DataRdyUSART()
#define OpenUART(a,b,c)		OpenUSART(a,b,c)
#define ReadUART()			ReadUSART()
#define WriteUART(a)		WriteUSART(a)
#define getsUART(a,b,c)		getsUSART(b,a)
#define putsUART(a)			putsUSART(a)
#define getcUART()			ReadUSART()
#define putcUART(a)			WriteUSART(a)
#define putrsUART(a)		putrsUSART((far rom char*)a)
**/
#endif // #ifndef HARDWARE_PROFILE_H
