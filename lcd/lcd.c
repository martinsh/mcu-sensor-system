/*
 *  LCD interface to HD44780
 *  Uses routines from delay.c
 *  Hardware connected as follows:
 *      R/W tied to GND (no read supported)
 *      RS  - RB15
 *      E   - RB14
 *
 *  Primary Crystal: 20Mhz.
 *	
 */
#include <xc.h>
#include "lcd.h"


#define FCY  10000000   // Fcy is 20/2 =  10 MHz
#include <libpic30.h>   // For timing delays




#define RS_PIN LATBbits.LATB15
#define EN_PIN LATBbits.LATB14
//#define RW_PIN LATBbits.LATB14 // R/W tied to GND. No read is necessary.

#define TRIS_RS               TRISBbits.TRISB15   /* TRIS for RS */
#define TRIS_E                TRISBbits.TRISB14    /* TRIS for E */


#define TRIS_DATA_PIN_7       TRISBbits.TRISB10
#define TRIS_DATA_PIN_6       TRISBbits.TRISB11
#define TRIS_DATA_PIN_5       TRISBbits.TRISB12
#define TRIS_DATA_PIN_4       TRISBbits.TRISB13

#define DATA_PIN_7           LATBbits.LATB10
#define DATA_PIN_6           LATBbits.LATB11
#define DATA_PIN_5           LATBbits.LATB12
#define DATA_PIN_4           LATBbits.LATB13


#define	LCD_STROBE()	((EN_PIN = 1),(EN_PIN=0))

/**
 * Write a byte to the LCD in 4 bit mode
 * @param c
 */
void lcd_write(unsigned char c)
{
        //RW_PIN = 0;   /* enable write */
	
	__delay_us(40);

        // The 4 high bits are transferred first
        DATA_PIN_7 = (((c >> 4) & 0b1000)>>3);
	DATA_PIN_6 = (((c >> 4) & 0b0100)>>2);
	DATA_PIN_5 = (((c >> 4) & 0b0010)>>1);
	DATA_PIN_4 = (((c >> 4) & 0b0001)>>0);

	LCD_STROBE();
	
        // the 4 lower bits
        DATA_PIN_7 = ((c & 0b1000)>>3);
	DATA_PIN_6 = ((c & 0b0100)>>2);
	DATA_PIN_5 = ((c & 0b0010)>>1);
	DATA_PIN_4 = ((c & 0b0001)>>0);


	LCD_STROBE();
}

/**
 * Write lower bits only.
 * @param c
 */
void lcd_write_lower_bits(unsigned char c){
        DATA_PIN_7 = ((c & 0b1000)>>3);
	DATA_PIN_6 = ((c & 0b0100)>>2);
	DATA_PIN_5 = ((c & 0b0010)>>1);
	DATA_PIN_4 = ((c & 0b0001)>>0);
}


/**
 * Clear and home the LCD
 */
void lcd_clear(void)
{
	RS_PIN = 0;
	lcd_write(0x1);
	__delay_ms(2);
}

/* write a string of chars to the LCD */

void lcd_puts(const char * s){
	RS_PIN = 1;	// write characters
	while(*s)
		lcd_write(*s++);
}

/* write one character to the LCD */

void lcd_putch(char c)
{
	RS_PIN = 1;	// write characters
	lcd_write( c );
}


void lcd_writeNumber(unsigned char number){

	unsigned char hund = 0;
	unsigned char tens = 0;

	while (number >= 100)
	{
		number -= 100;
		hund++;
	}

	while (number >= 10)
	{
		number -= 10;
		tens++;
	}

	if (hund == 0)
	{
		hund = 240;
		if (tens == 0)
			tens = 240;

	}


	lcd_putch(hund+48);
	lcd_putch(tens+48);
	lcd_putch(number+48);
}

/*
 * Go to the specified position
 * See datasheet "Set DDRAM Address"
 * For 2 line display:
 *   for 1st line: 0x00 - 0x27
 * 	 for 2nd line: 0x40 - 0x67
 */

void lcd_goto(unsigned char pos)
{
	RS_PIN = 0;
	lcd_write(0x80+pos);
}
	
/* initialise the LCD - put into 4 bit mode */
void lcd_init() {
	char init_value;

	init_value = 0x3;

        /* Make all control pins as outputs */
	TRIS_RS = 0;
	TRIS_E = 0;

	/* configure the common data pins as output */
	TRIS_DATA_PIN_7 = 0;
	TRIS_DATA_PIN_6 = 0;
	TRIS_DATA_PIN_5 = 0;
	TRIS_DATA_PIN_4 = 0;

	RS_PIN = 0;
	EN_PIN = 0;

	/* Allow a delay for POR.(minimum of 15ms) */
	__delay_ms(15);

        lcd_write_lower_bits(init_value);
	
	LCD_STROBE();
	__delay_ms(5);
	LCD_STROBE();
	__delay_us(200);
	LCD_STROBE();
	__delay_us(200);

        // Four bit mode
        lcd_write_lower_bits(0x2);

	LCD_STROBE();

        // From now on we can write whole byte
	lcd_write(0b101000); // Set 4 bits Mode. 2 lines.
	lcd_write(0b1100); // Display On, Cursor Off, No Blink
	lcd_clear();	// Clear screen
	lcd_write(0x6); // Set entry Mode
}
